# Hoja5.py
#
# Andrea Arguello 17801
# Luis Delgado 17187
# 23/02/2018

import simpy
import random

# Clase Process modela el comportamiento del proceso
def Process(numero, env,creation_time,memoriaresource,cpuresource):
    global tiempoTotalProcesos
    
    yield env.timeout(creation_time)

    horaInicio=env.now
    memoriaAOcupar=random.randint(1,10)
    inst_num=random.randint(1,10)
    #Inicia el proceso y ocupa memoria
    with memoria.get(memoriaAOcupar) as inMemoria:
        #yield inMemoria
        print ('%d llega a la memoria en %s a realizar %d instrucciones ocupando %d memoria' % (numero,horaInicio,inst_num,memoriaAOcupar))
        while (inst_num>0):
            with cpu.request() as inCPU: #lo manda al CPU
                yield inCPU
                yield env.timeout(1)
                print('%s llega a la memoria en %f' % (numero, env.now))
                inst_num-=3
                if (inst_num<0):
                    inst_num=0
                print('%s realiza 3 instrucciones, ahora tiene %d' %(numero,inst_num))
            if(inst_num>0):
                if(random.randint(1,2)==1):
                    with waiting.request() as inWaiting:
                        yield inWaiting
                        yield env.timeout(1)

        tiempoTotal=env.now-horaInicio
        print ('%s se tardo %f en terminar' % (numero,tiempoTotal))
        tiempoTotalProcesos+=tiempoTotal
                
                    
        
        
    
    
# Atributos
env = simpy.Environment() #inicia simulacion
memoria=simpy.Container(env,capacity=100);
cpu=simpy.Resource(env,capacity=1);
waiting=simpy.Resource(env,capacity=1);
interval=10
numProcess = 25
random.seed(10)

tiempoTotalProcesos=0
for i in range(numProcess):
    env.process(Process(i,env,random.expovariate(1.0/interval),memoria,cpu))

env.run()
print ("El tiempo total es", tiempoTotalProcesos, "y el tiempo promedio por proceso es", tiempoTotalProcesos/numProcess)
